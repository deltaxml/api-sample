import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.deltaxml.dita.DitaMapCompareProgressListener;

public class PercentageProgressListener implements DitaMapCompareProgressListener {

  private PrintWriter pw;
  private static final int TOTAL_NUM_OF_MANDATORY_ML_EVENTS= 8;
  private static final int TOTAL_NUM_OF_OPTIONAL_ML_EVENTS= 4;
  private int numOfTopicsToBeCompared; // n
  private int numOfTopicsToBeUpdated; // inserted+deleted

  private int totalNumOfMLEvents= TOTAL_NUM_OF_MANDATORY_ML_EVENTS; // initially
  private int numOfMLEventsRaised= 0;

  static class MutableInteger {
    public int value;
  }

  private Integer numberOfTopicComparisonStages= null;
  private Map<String, MutableInteger> topicComparisonStagesStartedAndCompleed= new HashMap<String, MutableInteger>();

  private int numOfTopicsUpdatesStartedAndCompleted;
  private boolean mapCopyStarted= false;

  private static final double ML_EVENTS_PERCENTAGE= 20;
  private static final double TOPIC_COMPARE_PERCENTAGE= 70;
  private static final double TOPIC_UPDATE_PERCENTAGE= 10;

  private final DecimalFormat df= new DecimalFormat("0.00");

  public PercentageProgressListener(Writer writer) {
    pw= new PrintWriter(writer, true);
  }

  public PercentageProgressListener(OutputStream outputStream) {
    this(new OutputStreamWriter(outputStream));
  }

  public PercentageProgressListener() {
    this(System.out);
  }

  private void reportPercentage() {
    double percentage= calculatePercentage();

    String percentageString= df.format(percentage);
    pw.println(percentageString);
  }

  private double calculatePercentage() {
    double mapEventPercentage= (numOfMLEventsRaised * ML_EVENTS_PERCENTAGE) / totalNumOfMLEvents;

    double topicUpdatePercentage= 0D;

    if (numOfTopicsToBeUpdated > 0) {
      topicUpdatePercentage= (numOfTopicsUpdatesStartedAndCompleted * TOPIC_UPDATE_PERCENTAGE) / (numOfTopicsToBeUpdated * 2);
    }

    double topicComparePercentage= 0D;

    if (numOfTopicsToBeCompared > 0) {
      double percentageAllocatedForEachTopicComparison= TOPIC_COMPARE_PERCENTAGE / numOfTopicsToBeCompared;
      for (MutableInteger mi : topicComparisonStagesStartedAndCompleed.values()) {
        topicComparePercentage+= (mi.value * percentageAllocatedForEachTopicComparison) / (numberOfTopicComparisonStages * 2);
      }
    }

    return mapEventPercentage + topicUpdatePercentage + topicComparePercentage;
  }

  private void addMapEvent() {
    numOfMLEventsRaised++;
    reportPercentage();
  }

  private void addTopicProgressEvent(ProcessingMode pm) {
    switch (pm) {
      case COMPARE:
        // Deliberately do nothing - the finer grained topic comparison events are used for progress monitoring.
        break;
      case COPY:
        // Need to look at what events occur when in this mode.
      case MARK_TOPIC:
        numOfTopicsUpdatesStartedAndCompleted++;
        reportPercentage();
        break;
    }
  }

  @Override
  public void inputMapCopyStart(DocumentSource source, String sourceHref, String destinationBaseHref) {
    if (!mapCopyStarted) {
      mapCopyStarted= true;
      totalNumOfMLEvents+= TOTAL_NUM_OF_OPTIONAL_ML_EVENTS;
    }
    addMapEvent();
  }

  @Override
  public void inputMapCopyFinish(DocumentSource source, String sourceHref, String destinationHref) {
    addMapEvent();
  }

  @Override
  public void inputMapScanStart(DocumentSource source, String sourceHref) {
    addMapEvent();
  }

  @Override
  public void inputMapScanFinish(DocumentSource source, String sourceHref, int foundTopicsCount) {
    addMapEvent();
  }

  @Override
  public void topicAlignmentStart() {
    addMapEvent();
  }

  @Override
  public void topicAlignmentFinish(Map<String, String> alignedTopics, Set<String> unalignedTopicsA,
                                   Set<String> unalignedTopicsB) {
    // we now know how many events we're expecting for
    // topic comparison and update
    numOfTopicsToBeCompared= alignedTopics.size();
    numOfTopicsToBeUpdated= unalignedTopicsA.size() + unalignedTopicsB.size();

    addMapEvent();
  }

  @Override
  public void processingTopicStart(String href, ProcessingMode processMode) {
    addTopicProgressEvent(processMode);
  }

  @Override
  public void processingTopicFinish(String href, ProcessingMode processMode) {
    addTopicProgressEvent(processMode);
  }

  @Override
  public void processingTopicFailure(String href, ProcessingMode processMode, Exception exception) {
    addTopicProgressEvent(processMode);
  }

  @Override
  public void topicCompareStages(String href, int totalNumberOfStages) {
    if (numberOfTopicComparisonStages == null) {
      numberOfTopicComparisonStages= totalNumberOfStages;
    } else if (numberOfTopicComparisonStages != totalNumberOfStages) {
      throw new Error(
                      "It should not be possible to have a different number of stages for the topic comarisons of a single map compare.");
    }
    topicComparisonStagesStartedAndCompleed.put(href, new MutableInteger());
  }

  @Override
  public void topicCompareStarted(String href, int stagesStarted, int stagesCompleted, String startedStageDescription) {
    topicComparisonStagesStartedAndCompleed.get(href).value++;
    reportPercentage();
  }

  @Override
  public void topicCompareCompleted(String href, int stagesStarted, int stagesCompleted, String completedStageDescription) {
    topicComparisonStagesStartedAndCompleed.get(href).value++;
    reportPercentage();
  }

  @Override
  public void topicCompareStageHeartbeat(String href) {
  }

  @Override
  public void mapLevelOutputProcessingStart() {
    addMapEvent();
  }

  @Override
  public void mapLevelOutputProcessingFinish() {
    addMapEvent();
  }

}
