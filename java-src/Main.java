
// Copyright (c) 2010 DeltaXML Ltd. All rights reserved.
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

class Main {
  /**
   * This is the main method of the Main class, which is invoked at the command line.
   *
   * @param args The array of command line arguments.
   */
  public static void main(String[] args) {
    if (args.length > 1) {
      usage();
    }
    if (args.length == 1) {
      String arg= args[0];

      if ("topic".equals(arg)) {
        TopicCompare.main(NO_ARGS);
      } else if ("mapfile".equals(arg)) {
        MapfileCompare.main(NO_ARGS);
      } else if ("mts".equals(arg) || "map-topicset".equals(arg)) {
        MapTopicsetCompare.main(NO_ARGS);
      } else if ("mts-inplace".equals(arg) || "map-topicset-inplace".equals(arg)) {
        MapTopicsetCompareInPlace.main(NO_ARGS);
      } else {
        usage();
      }
    } else {
      GeneralCompare.main(NO_ARGS);
    }
  }

  private static String[] NO_ARGS= new String[] {};

  private static void usage() {
    System.out.println("Usage: java -jar SimpleApiSample [ topic | mapfile | mts | mts-inplace ]");
    System.exit(1);
  }

  static File getSamplesDir(String sampleDirName) {
    // Find the top level samples directory
    File docdir= new File(System.getProperty("user.dir"));
    while (docdir.exists() && !docdir.getName().equals("samples")) {
      docdir= new File(docdir.getParent());
    }
    return new File(docdir, sampleDirName);
  }

  static Boolean copyDirectory(File source, File target) {
    Boolean success= false;
    try {
      if (source.isDirectory()) {
        target.mkdir();
        for (String child : source.list()) {
          copyDirectory(new File(source, child),
                        new File(target, child));
          success= true;
        }
      } else {

        InputStream inStream= new FileInputStream(source);
        OutputStream outStream= new FileOutputStream(target);

        int len;
        byte[] buffer= new byte[1024];
        while ((len= inStream.read(buffer)) > 0) {
          outStream.write(buffer, 0, len);
        }
        inStream.close();
        outStream.close();
      }
    } catch (IOException e) {
      System.out.println("Error: " + e.getMessage());
    }
    return success;
  }
}
