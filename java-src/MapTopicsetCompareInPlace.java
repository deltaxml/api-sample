
// Copyright (c) 2013 DeltaXML Ltd.  All rights reserved.
import java.io.File;

import com.deltaxml.dita.DeltaXMLDitaException;
import com.deltaxml.dita.DitaMapTopicsetCompare;
import com.deltaxml.dita.MapResultStructure;

class MapTopicsetCompareInPlace {
  /**
   * This is the main method of the MapCompareInPlace class, which is invoked at the command line.
   *
   * @param args The array of command line arguments.
   */
  public static void main(String[] args) {
    try {
      // Get a fresh comparison object
      DitaMapTopicsetCompare ditaCompare= new DitaMapTopicsetCompare();

      // Choose the topic-set map comparison result structure, and set the output location
      // to 'results/topic-set'.
      ditaCompare.setMapResultStructure(MapResultStructure.TOPIC_SET);
      File apisampleDir= Main.getSamplesDir("api-sample");
      File resultsDir= new File(apisampleDir, "results");
      resultsDir.mkdir();
      // Find input directory.
      File inputDir= Main.getSamplesDir("topicset-map-sample");
      File inputDir1= new File(inputDir, "doc1");
      File inputDir2= new File(inputDir, "doc2");
      // Create working copy
      File workingCopyLocation= new File(resultsDir, "map-sample-working-copy");
      workingCopyLocation.mkdir();
      File doc1Copy= new File(workingCopyLocation, "doc1");
      File doc2Copy= new File(workingCopyLocation, "doc2");
      Boolean success= Main.copyDirectory(inputDir1, doc1Copy);
      if (success) {
        success= Main.copyDirectory(inputDir2, doc2Copy);
      }
      if (success) {
        // Provide variables for each of the file-based compare method arguments
        File in1= new File(workingCopyLocation, "doc1/maps/main.ditamap");
        File in2= new File(workingCopyLocation, "doc2/maps/main.ditamap");

        // Run the compare (with pre/post commentary)
        System.out.println("Comparing files '" + in1 + "' and '" + in2 + "'");
        ditaCompare.compareInPlace(in1, in2);
        System.out.println("Overwriting contents in 'results/map-sample-working-copy'");
      }
    } catch (DeltaXMLDitaException e) {
      System.err.println(e.getMessage());
      e.printStackTrace(System.err);
    }
  }
}
