
// Copyright (c) 2013 DeltaXML Ltd. All rights reserved.
import java.io.File;

import com.deltaxml.dita.DeltaXMLDitaException;
import com.deltaxml.dita.DitaMapTopicsetCompare;
import com.deltaxml.dita.MapResultStructure;

class MapTopicsetCompare {
  /**
   * This is the main method of the MapCompare class, which is invoked at the command line.
   *
   * @param args The array of command line arguments.
   */
  public static void main(String[] args) {
    try {
      // Get a fresh comparison object
      DitaMapTopicsetCompare ditaCompare= new DitaMapTopicsetCompare();

      // Choose the topic-set map comparison result structure, and set the output location
      // to 'results/topic-set'.
      ditaCompare.setMapResultStructure(MapResultStructure.TOPIC_SET);
      File resultsDir= Main.getSamplesDir("api-sample");
      File mapCopyLocation= new File(resultsDir, "results/topic-set");

      // Find input directory.
      File inputDir= Main.getSamplesDir("topicset-map-sample");

      // Provide variables for each of the file-based compare method arguments
      File in1= new File(inputDir, "doc1/maps/main.ditamap");
      File in2= new File(inputDir, "doc2/maps/main.ditamap");

      // Run the compare (with pre/post commentary)
      System.out.println("Comparing files '" + in1 + "' and '" + in2 + "'");

      ditaCompare.compare(in1, in2, mapCopyLocation);
      System.out.println("Storing results in file 'results/topic-set/result-alias.ditamap'");
    } catch (DeltaXMLDitaException e) {
      System.err.println(e.getMessage());
      e.printStackTrace(System.err);
    }
  }
}
