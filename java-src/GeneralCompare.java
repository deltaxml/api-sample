
// Copyright (c) 2013 DeltaXML Ltd. All rights reserved.
import java.io.File;

import com.deltaxml.dita.DeltaXMLDitaException;
import com.deltaxml.dita.DitaCompare;
import com.deltaxml.dita.MapResultStructure;
import com.deltaxml.dita.OutputFormat;

class GeneralCompare {
  /**
   * This is the main method of the GeneralCompare class, which is invoked at the command line.
   *
   * @param args The array of command line arguments.
   */
  public static void main(String[] args) {
    try {
      // Get a fresh comparison object
      DitaCompare ditaCompare= new DitaCompare();

      // Choose the map-pair map comparison result structure, and set the output location
      // to 'results/map-pair'. Also specify that the differences within the topics are
      // represented using oXygen's tracked changes format.
      ditaCompare.setMapResultStructure(MapResultStructure.MAP_PAIR);

      File resultsDir= Main.getSamplesDir("api-sample");
      File mapCopyLocation= new File(resultsDir, "results/map-pair");
      ditaCompare.setOutputFormat(OutputFormat.OXYGEN_TCS);

      // Find input directory.
      File inputTopicDir= Main.getSamplesDir("topic-sample");
      File inputMapDir= Main.getSamplesDir("topicset-map-sample");

      // Provide variables for each of the file-based compare method arguments
      File inTopic1= new File(inputTopicDir, "in1.dita");
      File inTopic2= new File(inputTopicDir, "in2.dita");
      File outTopic= new File(new File("results"), "oxygen-tcs.dita");
      File inMap1= new File(inputMapDir, "doc1/maps/main.ditamap");
      File inMap2= new File(inputMapDir, "doc2/maps/main.ditamap");
      File outMapfile= new File(new File("results"), "oxygen-tcs.ditamap");

      // Run a topic compare (with pre/post commentary)
      System.out.println("Use the topic comparator to compare two DITA topic files:");
      System.out.println("  " + inTopic1);
      System.out.println("  " + inTopic2);
      ditaCompare.compareTopic(inTopic1, inTopic2, outTopic);
      System.out.println("Storing results in file '" + outTopic + "'");

      // Run a mapfile map compare (with pre/post commentary)
      System.out.println("Use the mapfile comparator to compare the XML content of two DITA map files:");
      System.out.println("  " + inMap1);
      System.out.println("  " + inMap2);
      ditaCompare.compareMapfile(inMap1, inMap2, outMapfile);
      System.out.println("Storing results in file '" + outMapfile + "'");

      // Run a topicset map compare (with pre/post commentary)
      System.out.println("Use the map-topicset comparator to compare the topics within two DITA map files:");
      System.out.println("  " + inMap1);
      System.out.println("  " + inMap2);
      ditaCompare.compareMapTopicset(inMap1, inMap2, mapCopyLocation);
      System.out.println("Storing results in file 'results/map-pair/result-alias.ditamap'");

    } catch (DeltaXMLDitaException e) {
      System.err.println(e.getMessage());
      e.printStackTrace(System.err);
    }
  }
}
