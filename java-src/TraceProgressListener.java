import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import com.deltaxml.dita.DitaMapCompareProgressListener;

public class TraceProgressListener implements DitaMapCompareProgressListener {

  private PrintWriter pw;
  private DateFormat dateFormat;

  public TraceProgressListener(Writer writer) {
    pw= new PrintWriter(writer, true);
    dateFormat= DateFormat.getTimeInstance();
  }

  public TraceProgressListener(OutputStream outputStream) {
    this(new OutputStreamWriter(outputStream));
  }

  public TraceProgressListener() {
    this(System.out);
  }

  @Override
  public void inputMapCopyStart(DocumentSource source, String sourceHref, String destinationBaseHref) {
    pw.println("Start map copy " + source + " from: " + sourceHref + " to: " + destinationBaseHref);
  }

  @Override
  public void inputMapCopyFinish(DocumentSource source, String sourceHref, String destinationHref) {
    pw.println("Stop map copy " + source + " from: " + sourceHref + " to: " + destinationHref);
  }

  @Override
  public void inputMapScanStart(DocumentSource source, String sourceHref) {
    pw.println("Start map scan " + source + " from: " + sourceHref);
  }

  @Override
  public void inputMapScanFinish(DocumentSource source, String sourceHref, int foundTopicsCount) {
    pw.println("Stop map scan " + source + " from: " + sourceHref + " for " + foundTopicsCount + " copies");
  }

  @Override
  public void topicAlignmentStart() {
    pw.println("Start topic alignment");
  }

  @Override
  public void topicAlignmentFinish(Map<String, String> alignedTopics, Set<String> unalignedTopicsA,
                                   Set<String> unalignedTopicsB) {
    pw.println("Stop topic alignment. Matches: " + alignedTopics.size() + " deletions: " + unalignedTopicsA
        .size() + " insertion: " + unalignedTopicsB.size());
  }

  @Override
  public void processingTopicStart(String href, ProcessingMode processMode) {
    pw.println("Start mode: " + processMode + " for topic: " + href);

  }

  @Override
  public void processingTopicFinish(String href, ProcessingMode processMode) {
    pw.println("Stop mode: " + processMode + " for topic: " + href);

  }

  @Override
  public void processingTopicFailure(String href, ProcessingMode processMode, Exception exception) {
    pw.println("Error during mode: " + processMode + " for topic: " + href + " error message: " + exception.getMessage());
  }

  @Override
  public void topicCompareStages(String href, int totalNumberOfStages) {
    pw.println("Comparing topic " + href + " in " + totalNumberOfStages + " stages");
  }

  @Override
  public void topicCompareStarted(String href, int stagesStarted, int stagesCompleted, String startedStageDescription) {
    pw.print("Stage '" + startedStageDescription);
    pw.print("' started for " + href);
    pw.print(" [Started stages " + stagesStarted);
    pw.println(", Completed stages " + stagesCompleted + "]");
  }

  @Override
  public void topicCompareCompleted(String href, int stagesStarted, int stagesCompleted, String completedStageDescription) {
    pw.print("Stage '" + completedStageDescription);
    pw.print("' completed for " + href);
    pw.print(" [Started stages " + stagesStarted);
    pw.println(", Completed stages " + stagesCompleted + "]");
  }

  @Override
  public void topicCompareStageHeartbeat(String href) {
    Date dt= new Date(System.currentTimeMillis());
    pw.println("Heartbeat at " + dateFormat.format(dt) + " for href: " + href);
  }

  @Override
  public void mapLevelOutputProcessingStart() {
    pw.println("Start processing map level output");
  }

  @Override
  public void mapLevelOutputProcessingFinish() {
    pw.println("Stop processing map level output");
  }

}
