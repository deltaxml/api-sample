
// Copyright (c) 2013 DeltaXML Ltd. All rights reserved.
import java.io.File;
import java.io.FileNotFoundException;

import com.deltaxml.dita.DeltaXMLDitaException;
import com.deltaxml.dita.DitaMapfileCompare;
import com.deltaxml.dita.IndentOutput;

class MapfileCompare {
  /**
   * This is the main method of the TopicCompare class, which is invoked at the command line.
   *
   * @param args The array of command line arguments.
   */
  public static void main(String[] args) {
    try {
      // Get a fresh comparison object
      DitaMapfileCompare ditaCompare= new DitaMapfileCompare();

      // Enable the output indentation option/parameter
      ditaCompare.setIndentOutput(IndentOutput.YES);

      // Find input directory.
      File inputDir= Main.getSamplesDir("mapfile-map-sample");

      // Provide variables for each of the file-based compare method arguments
      File in1= new File(inputDir, "in1.ditamap");
      File in2= new File(inputDir, "in2.ditamap");
      File out= new File(new File("results"), "markup.ditamap");

      // Run the compare (with pre/post commentary)
      System.out.println("Comparing files '" + in1 + "' and '" + in2 + "'");
      ditaCompare.compare(in1, in2, out);
      System.out.println("Storing results in file '" + out + "'");
    } catch (FileNotFoundException e) {
      System.err.println(e.getMessage());
      e.printStackTrace(System.err);
    } catch (DeltaXMLDitaException e) {
      System.err.println(e.getMessage());
      e.printStackTrace(System.err);
    }
  }
}
