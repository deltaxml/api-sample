import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.text.DateFormat;

import com.deltaxml.dita.DitaTopicCompareProgressListener;

public class TopicProgressListener implements DitaTopicCompareProgressListener {

  private PrintWriter pw;
  private DateFormat dateFormat;

  public TopicProgressListener(Writer writer) {
    pw= new PrintWriter(writer, true);
    dateFormat= DateFormat.getTimeInstance();
  }

  public TopicProgressListener(OutputStream outputStream) {
    this(new OutputStreamWriter(outputStream));
  }

  public TopicProgressListener() {
    this(System.out);
  }

  @Override
  public void stages(int totalNumberOfStages) {
    pw.println("Number of topic stages: " + totalNumberOfStages);
  }

  @Override
  public void started(int stagesStarted, int stagesCompleted, String stageDescription) {
    pw.println("Started: [started] " + stagesStarted + " [completed] " + stagesCompleted + " for " + stageDescription);
  }

  @Override
  public void completed(int stagesStarted, int stagesCompleted, String stageDescription) {
    pw.println("Completed: [started] " + stagesStarted + " [completed] " + stagesCompleted + " for " + stageDescription);
  }

  @Override
  public void compareStageHeartbeat() {
    pw.println("Heartbeat");

  }

}
